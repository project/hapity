<?php

/**
 * @file
 * Template file for the Hapity list broad cast list.
 */

/**
 * Available variables.
 *
 * $broadcasts
 *   An array of broadcasts nodes.
 */
?>
<?php if(!empty($broadcasts)): ?>
  <?php foreach($broadcasts as $node): ?>
    <?php $title = $node->title;?>
    <h2><?php print l($title, 'node/' . $node->nid); ?></h2>
    <p><?php print $node->body[LANGUAGE_NONE][0]['value'];?></p>
  <?php endforeach; ?>
<?php endif; ?>
