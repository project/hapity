CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Module Details
 * Dependencies
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

Hapity module allows a user to auto post their hapity.com broadcasts
automatically to their drupal website.

MODULE DETAILS
--------------

This module uses hapity.com APIs to auto post hapity broadcasts.
This module creates hapity type node. This module insetrs hapity
broadcast embed code into the node body field.

DEPENDENCIES
------------

* This module has no dependency. 

INSTALLATION
-------------

* Download the hapity module.
* Enable the module and configure hapity plugin ID.

CONFIGURATIONS
--------------
* Go to hapity.com under settings get your 'Your Plugin's ID' and
  Configure it in /admin/config/content/hapity.
* Once you have configued hapity module correctally, create
  broadcast using hapity.com, Android or iOS App.
* Broadcase will be posted to your drupal website automatically.

TROUBLESHOOTING
---------------

Hapity works only with correct plugin ID.
Make sure your plugin ID is correct.
Please check your plugin id at http://www.hapity.com/main/settings.
In case the module is not working, please check :
* /admin/config/content/hapity - for correct 'Your Plugin's ID'.
* Clear cache with change in rules.
* Reinstall the module after disable and uninstallation.

MAINTAINERS
-----------

Current maintainers:

 * Khurram Awan https://www.drupal.org/u/khurram_awan

This project has been sponsored by:
 * Hapity.com
